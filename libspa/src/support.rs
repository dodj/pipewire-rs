//! Types and methods to wrap the "support" standard plugin.
//!
//! TODO make it so you can set the global logger to a `Log`. This would enable log messages crated
//! by dependencies to log to the SPA logger.

use log::{Level, LevelFilter};
use spa_sys::{
    itimerspec, spa_cpu, spa_cpu_methods, spa_log, spa_log_methods, spa_loop, spa_node,
    spa_node_methods, spa_poll_event, spa_system, spa_system_methods, timespec,
};
use std::{
    convert::TryInto,
    io,
    os::raw::{c_int, c_void},
};

use crate::{interface::Interface, SpaResult};

// Log

pub struct Log<'a> {
    raw: &'a mut spa_log,
}

unsafe impl<'a> Interface<'a> for Log<'a> {
    const NAME: &'static [u8] = b"Spa:Pointer:Interface:Log\0";
    const VERSION: u32 = 0;
    type Type = spa_log;

    fn from_raw(raw: &'a mut spa_log) -> Self {
        Log { raw }
    }
}

impl<'a> Log<'a> {
    /// The lowest level of log messages that will be displayed.
    pub fn level(&self) -> LevelFilter {
        match self.raw.level {
            0 => LevelFilter::Off,
            1 => LevelFilter::Error,
            2 => LevelFilter::Warn,
            3 => LevelFilter::Info,
            4 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        }
    }

    pub fn set_level(&mut self, level: LevelFilter) {
        let level = match level {
            LevelFilter::Off => 0,
            LevelFilter::Error => 1,
            LevelFilter::Warn => 2,
            LevelFilter::Info => 3,
            LevelFilter::Debug => 4,
            LevelFilter::Trace => 5,
        };
        self.raw.level = level;
    }

    /// Log a message.
    ///
    /// Don't use this function directly: instead use the macros. This function always prints the
    /// message, regarless of the level filter.
    ///
    /// # Safety
    ///
    /// The `file` string must be null-terminated.
    ///
    /// # Panics
    ///
    /// The function will panic if `msg` contains interior null bytes.
    #[doc(hidden)]
    pub unsafe fn _log(&mut self, level: Level, file: &'static str, line: u32, msg: String) {
        let mut msg = msg.into_bytes();
        // CString would panic on interior null bytes, we just pass this string through to display
        // up to the null byte. Also add a newline to match `log` crate behavior`.
        msg.push(b'\n');
        msg.push(b'\0');
        let level = match level {
            Level::Error => 1,
            Level::Warn => 2,
            Level::Info => 3,
            Level::Debug => 4,
            Level::Trace => 5,
        };
        crate::spa_interface_call_method!(
            self.raw as *mut spa_log,
            spa_log_methods,
            log,
            level,
            file as *const str as *mut _, // Safety: user responsibility to be null-terminated.
            line.try_into().unwrap(),
            b"-\0" as *const [u8; 2] as *mut _, // unsupported.
            msg.as_ptr() as *const _
        )
    }
}

/// Log a message
/// Use the other macros (`error`, `warn`, `info`, `debug`, `trace`) to avoid having to specify a level.
#[macro_export]
macro_rules! log {
    ($logger:expr, $level:expr, $($fmt_args:tt)+) => {
        // TODO mark branch unlikely once rustc supports this (currently only possible using
        // nightly-only intrinsics)
        if $level <= $logger.level() {
            // TODO currently allocates. I can't see how to bridge between `println` and `printf`
            // semantics for formatting text withouta allocating.
            let msg = format!($($fmt_args)+);
            unsafe { $logger._log($level, concat!(file!(), "\0"), line!(), msg) }
        }
    }
}

/// Log an error
#[macro_export]
macro_rules! error {
    ($logger:expr, $($fmt_args:tt)+) => {
        $crate::log!($logger, log::Level::Error, $($fmt_args)+)
    }
}

/// Log a warning
#[macro_export]
macro_rules! warn {
    ($logger:expr, $($fmt_args:tt)+) => {
        $crate::log!($logger, log::Level::Warn, $($fmt_args)+)
    }
}

/// Log some information
#[macro_export]
macro_rules! info {
    ($logger:expr, $($fmt_args:tt)+) => {
        $crate::log!($logger, log::Level::Info, $($fmt_args)+)
    }
}

/// Log some debug information
#[macro_export]
macro_rules! debug {
    ($logger:expr, $($fmt_args:tt)+) => {
        $crate::log!($logger, log::Level::Debug, $($fmt_args)+)
    }
}

/// Log some detailed debug information.
#[macro_export]
macro_rules! trace {
    ($logger:expr, $($fmt_args:tt)+) => {
        $crate::log!($logger, log::Level::Trace, $($fmt_args)+)
    }
}

// System

/// Access to syscalls.
pub struct System<'a> {
    raw: &'a mut spa_system,
}

unsafe impl<'a> Interface<'a> for System<'a> {
    const NAME: &'static [u8] = b"Spa:Pointer:Interface:System\0";
    const VERSION: u32 = 0;
    type Type = spa_system;

    fn from_raw(raw: &'a mut spa_system) -> Self {
        System { raw }
    }
}

macro_rules! sys_method {
    ($lit:expr, $name:ident ($($arg:ident : $argty:ty),*) -> $ret:ty) => {
        #[doc = "Access to the `"]
        #[doc = $lit]
        #[doc = "` syscall."]
        #[doc = ""]
        #[doc = "# Safety"]
        #[doc = ""]
        #[doc = "The safety requirements are the same as for the underlying syscall."]
        pub unsafe fn $name(&mut self, $($arg : $argty),*) -> $ret {
            crate::spa_interface_call_method!(
                self.raw as *mut spa_system,
                spa_system_methods,
                $name,
                $($arg),*
            )
        }
    };

    ($lit:expr, $name:ident ($($arg:ident : $argty:ty),*)) => {
        sys_method!($lit, $name($($arg : $argty),*) -> ());
    };
}

impl<'a> System<'a> {
    sys_method!("read", read(fd: c_int, buf: *mut c_void, count: u64) -> i64);
    sys_method!("write", write(fd: c_int, buf: *const c_void, count: u64) -> i64);
    // TODO can we write this for variadics?
    //sys_method!("ioctl", write(fd: c_int, request: c_ulong, ...) -> i64);
    sys_method!("close", close(fd: c_int) -> c_int);
    sys_method!("clock_gettime",
        clock_gettime(clockid: c_int, value: *mut timespec) -> c_int);
    sys_method!("clock_getres", clock_getres(clockid: c_int, res: *mut timespec) -> c_int);
    sys_method!("pollfd_create", pollfd_create(flags: c_int) -> c_int);
    sys_method!("pollfd_add",
        pollfd_add(pfd: c_int, fd: c_int, events: u32, data: *mut c_void) -> c_int);
    sys_method!("pollfd_mod",
        pollfd_mod(pfd: c_int, fd: c_int, events: u32, data: *mut c_void) -> c_int);
    sys_method!("pollfd_del", pollfd_del(pfd: c_int, fd: c_int) -> c_int);
    sys_method!("pollfd_wait",
        pollfd_wait(pfd: c_int, ev: *mut spa_poll_event, n_ev: c_int, timeout: c_int) -> c_int);
    sys_method!("timerfd_create", timerfd_create(clockid: c_int, flags: c_int) -> c_int);
    sys_method!("timerfd_settime", timerfd_settime(
        fd: c_int,
        flags: c_int,
        new_value: *const itimerspec,
        old_value: *mut itimerspec
    ) -> c_int);
    sys_method!("timerfd_gettime",
        timerfd_gettime(fd: c_int, curr_value: *mut itimerspec) -> c_int);
    sys_method!("timerfd_read", timerfd_read(fd: c_int, expirations: *mut u64) -> c_int);
    sys_method!("eventfd_create", eventfd_create(flags: c_int) -> c_int);
    sys_method!("eventfd_write", eventfd_write(fd: c_int, count: u64) -> c_int);
    sys_method!("eventfd_read", eventfd_read(fd: c_int, count: *mut u64) -> c_int);
    sys_method!("signalfd_create", signalfd_create(signal: c_int, flags: c_int) -> c_int);
    sys_method!("signalfd_read", signalfd_read(fd: c_int, signal: *mut c_int) -> c_int);
}

// CPU

/// The CPU support object.
pub struct Cpu<'a> {
    raw: &'a mut spa_cpu,
}

unsafe impl<'a> Interface<'a> for Cpu<'a> {
    const NAME: &'static [u8] = b"Spa:Pointer:Interface:CPU\0";
    const VERSION: u32 = 0;
    type Type = spa_cpu;

    fn from_raw(raw: &'a mut spa_cpu) -> Self {
        Cpu { raw }
    }
}

impl<'a> Cpu<'a> {
    /// Get the CPU flags.
    pub fn flags(&mut self) -> u32 {
        unsafe {
            crate::spa_interface_call_method!(self.raw as *mut spa_cpu, spa_cpu_methods, get_flags,)
        }
    }

    /// Set the CPU force flags.
    pub fn force_flags(&mut self, flags: u32) -> io::Result<()> {
        SpaResult::from_raw(unsafe {
            crate::spa_interface_call_method!(
                self.raw as *mut spa_cpu,
                spa_cpu_methods,
                force_flags,
                flags
            )
        })
        .into_sync_result()
        .map(|_| ())
    }

    /// Get the max number of parallel threads (the number of cores).
    ///
    /// This is a 'best effort' and should not be relied upon as correct.
    pub fn count(&mut self) -> u32 {
        unsafe {
            crate::spa_interface_call_method!(self.raw as *mut spa_cpu, spa_cpu_methods, get_count,)
        }
    }

    /// The maximum alignment that the CPU uses.
    ///
    /// This can be useful when allocating memory, where the type of the data (and therefore the
    /// CPU instructions used to manipulate it) is not known. `malloc` uses this approach.
    pub fn max_align(&mut self) -> u32 {
        unsafe {
            crate::spa_interface_call_method!(
                self.raw as *mut spa_cpu,
                spa_cpu_methods,
                get_max_align,
            )
        }
    }
}

// Loop

pub struct Loop<'a> {
    raw: &'a mut spa_loop,
}

unsafe impl<'a> Interface<'a> for Loop<'a> {
    const NAME: &'static [u8] = b"Spa:Pointer:Interface:Loop\0";
    const VERSION: u32 = 0;
    type Type = spa_loop;

    fn from_raw(raw: &'a mut spa_loop) -> Self {
        Loop { raw }
    }
}

/*
impl<'a> Loop<'a> {
    pub fn add_soiurce
}
*/

// Node

pub struct Node<'a> {
    raw: &'a mut spa_node,
}

unsafe impl<'a> Interface<'a> for Node<'a> {
    const NAME: &'static [u8] = b"Spa:Pointer:Interface:Node\0";
    const VERSION: u32 = 0;
    type Type = spa_node;

    fn from_raw(raw: &'a mut spa_node) -> Self {
        Node { raw }
    }
}
