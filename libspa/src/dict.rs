use bitflags::bitflags;
use std::{
    alloc,
    convert::{TryFrom, TryInto},
    ffi::CStr,
    fmt,
    mem::{self, MaybeUninit},
    ptr, slice,
};

/// A reference to an immutable spa dictionary.
#[repr(transparent)]
#[derive(Copy, Clone)]
pub struct Dict<'a> {
    raw: &'a spa_sys::spa_dict,
}

impl<'a> Dict<'a> {
    /// Wraps the provided pointer in a read-only `Dict` struct without taking ownership of the
    /// struct pointed to.
    ///
    /// # Safety
    ///
    /// - The provided pointer must point to a valid, well-aligned `spa_dict` struct, and must not
    ///   be `NULL`.
    /// - The struct pointed to must be kept valid for the entire lifetime of the created `Dict`.
    ///
    /// Violating any of these rules will result in undefined behaviour.
    pub unsafe fn from_ptr(dict: *const spa_sys::spa_dict) -> Self {
        assert!(
            !dict.is_null(),
            "Dict must not be created from a pointer that is NULL"
        );

        Self { raw: &*dict }
    }
    /// Obtain the pointer to the raw `spa_dict` struct.
    ///
    /// The pointer will be vaild for the lifetime `'a`.
    fn into_raw(self) -> *const spa_sys::spa_dict {
        self.raw as *const _
    }

    /// An iterator over all raw key-value pairs.
    /// The iterator element type is `(&CStr, &CStr)`.
    fn iter_cstr(&self) -> CIter {
        CIter { rest: self.items() }
    }

    /// An iterator over all key-value pairs that are valid utf-8.
    /// The iterator element type is `(&str, &str)`.
    fn iter(&self) -> Iter {
        Iter {
            inner: self.iter_cstr(),
        }
    }

    /// An iterator over all keys that are valid utf-8.
    /// The iterator element type is &str.
    fn keys(&self) -> Keys {
        Keys {
            inner: self.iter_cstr(),
        }
    }

    /// An iterator over all values that are valid utf-8.
    /// The iterator element type is &str.
    fn values(&self) -> Values {
        Values {
            inner: self.iter_cstr(),
        }
    }

    /// Returns the number of key-value-pairs in the dict.
    /// This is the number of all pairs, not only pairs that are valid-utf8.
    pub fn len(&self) -> usize {
        self.raw.n_items.try_into().unwrap()
    }

    /// Returns `true` if the dict is empty, `false` if it is not.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Returns the bitflags that are set for the dict.
    pub fn flags(&self) -> Flags {
        Flags::from_bits_truncate(self.raw.flags)
    }

    /// Get the value associated with the provided key.
    ///
    /// If the dict does not contain the key or the value is non-utf8, `None` is returned.
    /// Use [`iter_cstr`] if you need a non-utf8 key or value.
    ///
    /// [`iter_cstr`]: #method.iter_cstr
    // FIXME: Some items might be integers, booleans, floats, doubles or pointers instead of strings.
    // Perhaps we should return an enum that can be any of these values.
    // See https://gitlab.freedesktop.org/pipewire/pipewire-rs/-/merge_requests/12#note_695914.
    pub fn get(&self, key: &str) -> Option<&str> {
        self.iter().find(|(k, _)| *k == key).map(|(_, v)| v)
    }

    /// Get the items as a slice (we can do this as they are contiguous in memory).
    fn items(&self) -> &[DictItem] {
        let n_items = usize::try_from(self.raw.n_items).unwrap();
        // Safety: slice cannot outlive `self`.
        unsafe { slice::from_raw_parts(self.raw.items as *const _, n_items) }
    }

    /// Convert this reference into an owned dictionary.
    pub fn to_owned(&self) -> OwnedDict {
        let mut owned = OwnedDict::with_capacity(self.len());
        for item in self.items().iter() {
            owned.insert(item.key().to_bytes(), item.value().to_bytes());
        }
        owned
    }
}

/// Create a `spa_dict` compatible object that we own.
pub struct OwnedDict {
    raw: spa_sys::spa_dict,
    // If greater than raw.n_items, then we have space to store more values without reallocating.
    capacity: u32,
}

impl fmt::Debug for OwnedDict {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.as_dict().fmt(f)
    }
}

impl OwnedDict {
    pub fn new() -> Self {
        Self {
            raw: spa_sys::spa_dict {
                flags: 0,
                n_items: 0,
                items: ptr::null(),
            },
            capacity: 0,
        }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        let mut dict = Self::new();
        dict.realloc(
            capacity
                .checked_next_power_of_two()
                .unwrap()
                .try_into()
                .unwrap(),
        );
        dict
    }

    /// Insert the key-value pair, overwriting any old value.
    pub fn insert(&mut self, key: impl AsRef<[u8]>, value: impl AsRef<[u8]>) {
        let key = key.as_ref();
        // check if the key is already present.
        if let Some(item) = self
            .items_mut()
            .iter_mut()
            .find(|item| item.key().to_bytes() == key)
        {
            item.replace_value(value);
        } else {
            if self.capacity == self.raw.n_items {
                if self.capacity == 0 {
                    self.realloc(4);
                } else {
                    self.realloc(self.capacity * 2);
                }
            }
            let new_item = DictItem::new(key, value);
            unsafe {
                let len = self.len();
                self.cap_as_array()[len].as_mut_ptr().write(new_item);
                self.raw.n_items += 1;
            }
        }
    }

    /// Remove the key-value pair if it exists.
    pub fn remove(&mut self, key: impl AsRef<[u8]>) -> Option<DictItem> {
        let key = key.as_ref();
        if let Some(idx) = self
            .items()
            .iter()
            .position(|item| item.key().to_bytes() == key)
        {
            unsafe {
                // Grab copy of item
                let item_ptr = self.cap_as_array()[idx].as_ptr();
                let item = item_ptr.read();
                if idx + 1 < self.len() {
                    // shift rest down by 1
                    let ptr_next = self.cap_as_array()[idx + 1].as_ptr();
                    ptr::copy(ptr_next, item_ptr as *mut _, self.len() - (idx + 1));
                }
                self.raw.n_items -= 1;
                Some(item)
            }
        } else {
            None
        }
    }

    /// Clear the object, removing all key-value pairs.
    pub fn clear(&mut self) {
        self.realloc(0);
    }

    /// The number of items in the dictionary.
    pub fn len(&self) -> usize {
        self.raw.n_items.try_into().unwrap()
    }

    /// The available space for items in the dictionary.
    pub fn capacity(&self) -> usize {
        self.capacity.try_into().unwrap()
    }

    /// We allocate items as an array, so interpreting the data as such is valid, and the correct
    /// way to access data. Using `offset` and `size_of` would ignore padding if present.
    fn cap_as_array(&mut self) -> &mut [MaybeUninit<DictItem>] {
        unsafe { slice::from_raw_parts_mut(self.raw.items as *mut _, self.capacity()) }
    }

    /// The contents of the dictionary.
    pub fn items(&self) -> &[DictItem] {
        unsafe { slice::from_raw_parts(self.raw.items as *const _ as *const DictItem, self.len()) }
    }

    /// This is private as it can be used to break uniqueness of keys.
    fn items_mut(&mut self) -> &mut [DictItem] {
        unsafe { slice::from_raw_parts_mut(self.raw.items as *mut DictItem, self.len()) }
    }

    /// Allocate memory to match given size.
    ///
    /// size == 0 indicates that we should dealloc.
    fn realloc(&mut self, new_cap: u32) {
        use std::{
            alloc::{alloc, dealloc, realloc, Layout},
            mem::{align_of, size_of},
        };

        let new_cap_s = usize::try_from(new_cap).unwrap();
        let curr_cap_s = usize::try_from(self.capacity).unwrap();
        let n_items_s = usize::try_from(self.raw.n_items).unwrap();

        unsafe {
            if self.capacity == 0 && new_cap > 0 {
                // alloc
                let new_layout = Layout::array::<DictItem>(new_cap_s).unwrap();
                self.raw.items = alloc(new_layout) as *mut _;
                self.capacity = new_cap.try_into().unwrap();
            } else if self.capacity > 0 && new_cap == 0 {
                // dealloc
                let curr_layout = Layout::array::<DictItem>(curr_cap_s).unwrap();
                if self.raw.n_items > 0 {
                    // drop items past new capacity
                    for item in &self.cap_as_array()[..n_items_s] {
                        // move the value and drop it.
                        let _ = item.as_ptr().read();
                    }
                    self.raw.n_items = 0;
                }
                dealloc(self.raw.items as *mut _, curr_layout);
                self.capacity = 0;
            } else if self.capacity > 0 && new_cap > 0 {
                // realloc
                let curr_layout = Layout::array::<DictItem>(curr_cap_s).unwrap();
                let new_layout = Layout::array::<DictItem>(new_cap_s).unwrap();
                if self.raw.n_items > new_cap {
                    // drop items past new capacity
                    for item in &self.cap_as_array()[new_cap_s..n_items_s] {
                        // move the value and drop it.
                        let _ = item.as_ptr().read();
                    }
                    self.raw.n_items = new_cap;
                }
                self.raw.items =
                    realloc(self.raw.items as *mut _, curr_layout, new_layout.size()) as *mut _;
                self.capacity = new_cap;
            }
            // case self.capacity == 0 && new_cap == 0 we have nothing to do.
        }
    }

    pub fn as_dict(&self) -> Dict {
        Dict { raw: &self.raw }
    }
}

impl fmt::Debug for Dict<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        struct DebugItems<'a>(&'a Dict<'a>);
        impl fmt::Debug for DebugItems<'_> {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                f.debug_map().entries(self.0.iter_cstr()).finish()
            }
        }

        f.debug_struct("Dict")
            .field("flags", &self.flags())
            .field("items", &DebugItems(self))
            .finish()
    }
}

/// Identical in layout to `spa_sys::spa_dict_item`, but deallocates its memory on drop (only if
/// owned).
#[repr(transparent)]
pub struct DictItem {
    raw: spa_sys::spa_dict_item,
}

impl DictItem {
    /// Create a new DictItem from raw parts.
    fn new(key: impl AsRef<[u8]>, value: impl AsRef<[u8]>) -> Self {
        use std::alloc::{alloc, Layout};
        let key = key.as_ref();
        let value = value.as_ref();

        let mut item = Self {
            raw: spa_sys::spa_dict_item {
                key: ptr::null(),
                value: ptr::null(),
            },
        };
        item.alloc_key(key);
        item.alloc_value(value);
        item
    }

    /// Get the key from this item.
    pub fn key(&self) -> &CStr {
        unsafe { CStr::from_ptr(self.raw.key) }
    }

    /// Get the value from this item.
    pub fn value(&self) -> &CStr {
        unsafe { CStr::from_ptr(self.raw.value) }
    }

    fn replace_value(&mut self, new_value: impl AsRef<[u8]>) {
        let new_value = new_value.as_ref();
        assert!(
            new_value.iter().find(|byte| **byte == 0).is_none(),
            "found interior null byte"
        );
        unsafe {
            self.dealloc_value();
            self.alloc_value(new_value);
        };
    }

    /// If another key was already allocated, it will leak (unless deallocated).
    fn alloc_key(&mut self, key: &[u8]) {
        use std::alloc::{alloc, Layout};
        assert!(
            key.iter().find(|byte| **byte == 0).is_none(),
            "found interior null byte"
        );
        let key_layout = Layout::from_size_align(key.len() + 1, 1).unwrap();
        let key_mem = unsafe {
            let key_mem = alloc(key_layout);
            ptr::copy_nonoverlapping(key.as_ptr(), key_mem, key.len());
            // write null byte
            key_mem.offset(key.len().try_into().unwrap()).write(0);
            key_mem
        };
        self.raw.key = key_mem as *mut _;
    }

    /// If another value was already allocated, it will leak (unless deallocated).
    fn alloc_value(&mut self, value: &[u8]) {
        use std::alloc::{alloc, Layout};
        assert!(
            value.iter().find(|byte| **byte == 0).is_none(),
            "found interior null byte"
        );
        let value_layout = Layout::from_size_align(value.len() + 1, 1).unwrap();
        let value_mem = unsafe {
            let value_mem = alloc(value_layout);
            ptr::copy_nonoverlapping(value.as_ptr(), value_mem, value.len());
            // write null byte
            value_mem.offset(value.len().try_into().unwrap()).write(0);
            value_mem
        };
        self.raw.value = value_mem as *mut _;
    }

    /// After this function is called, don't access the value until it is set again.
    unsafe fn dealloc_key(&mut self) {
        use std::alloc::{dealloc, Layout};
        let key = CStr::from_ptr(self.raw.key);
        let key_layout = Layout::from_size_align(key.to_bytes_with_nul().len(), 1).unwrap();
        dealloc(self.raw.key as *mut _, key_layout);
    }

    /// After this function is called, don't access the value until it is set again.
    unsafe fn dealloc_value(&mut self) {
        use std::alloc::{dealloc, Layout};
        let value = CStr::from_ptr(self.raw.value);
        let value_layout = Layout::from_size_align(value.to_bytes_with_nul().len(), 1).unwrap();
        dealloc(self.raw.value as *mut _, value_layout);
    }
}

impl Drop for DictItem {
    fn drop(&mut self) {
        use std::alloc::{dealloc, Layout};

        unsafe {
            self.dealloc_key();
            self.dealloc_value();
        }
    }
}

bitflags! {
    pub struct Flags: u32 {
        // These flags are redefinitions from
        // https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/include/spa/utils/dict.h
        const SORTED = spa_sys::SPA_DICT_FLAG_SORTED;
    }
}

pub struct CIter<'a> {
    rest: &'a [DictItem],
}

impl<'a> Iterator for CIter<'a> {
    type Item = (&'a CStr, &'a CStr);

    fn next(&mut self) -> Option<Self::Item> {
        match self.rest.split_first() {
            Some((item, rest)) => {
                self.rest = rest;
                Some((item.key(), item.value()))
            }
            None => None,
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        // We know the exact value, so lower bound and upper bound are the same.
        (self.rest.len(), Some(self.rest.len()))
    }
}

pub struct Iter<'a> {
    inner: CIter<'a>,
}

impl<'a> Iterator for Iter<'a> {
    type Item = (&'a str, &'a str);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner
            .find_map(|(k, v)| k.to_str().ok().zip(v.to_str().ok()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        // Lower bound is 0, as all keys left might not be valid UTF-8.
        (0, self.inner.size_hint().1)
    }
}

pub struct Keys<'a> {
    inner: CIter<'a>,
}

impl<'a> Iterator for Keys<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.find_map(|(k, _)| k.to_str().ok())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, self.inner.size_hint().1)
    }
}

pub struct Values<'a> {
    inner: CIter<'a>,
}

impl<'a> Iterator for Values<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.find_map(|(_, v)| v.to_str().ok())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, self.inner.size_hint().1)
    }
}

#[cfg(test)]
mod tests {
    use super::{Dict, DictItem, Flags, OwnedDict};
    use spa_sys::{spa_dict, spa_dict_item};
    use std::{
        ffi::{CStr, CString},
        ptr,
    };

    /// Create a raw dict with the specified number of key-value pairs.
    ///
    /// `num_items` must not be zero, or this function will panic.
    ///
    /// Each key value pair is `("K<n>", "V<n>")`, with *\<n\>* being an element of the range `0..num_items`.
    ///
    /// The function returns a tuple consisting of:
    /// 1. An allocation (`Vec`) containing the raw Key and Value Strings.
    /// 2. An allocation (`Vec`) containing all the items.
    /// 3. The created `spa_dict` struct.
    ///
    /// The first two items must be kept alive for the entire lifetime of the returned `spa_dict` struct.
    fn make_raw_dict(
        num_items: u32,
    ) -> (
        Vec<(CString, CString)>,
        Vec<spa_dict_item>,
        spa_sys::spa_dict,
    ) {
        assert!(num_items != 0, "num_items must not be zero");

        let mut strings: Vec<(CString, CString)> = Vec::with_capacity(num_items as usize);
        let mut items: Vec<spa_dict_item> = Vec::with_capacity(num_items as usize);

        for i in 0..num_items {
            let k = CString::new(format!("K{}", i)).unwrap();
            let v = CString::new(format!("V{}", i)).unwrap();
            let item = spa_dict_item {
                key: k.as_ptr(),
                value: v.as_ptr(),
            };
            strings.push((k, v));
            items.push(item);
        }

        let raw = spa_dict {
            flags: Flags::empty().bits,
            n_items: num_items,
            items: items.as_ptr(),
        };

        (strings, items, raw)
    }

    #[test]
    fn test_empty_dict() {
        let raw = spa_dict {
            flags: Flags::empty().bits,
            n_items: 0,
            items: ptr::null(),
        };

        let dict = unsafe { Dict::from_ptr(&raw) };
        let iter = dict.iter_cstr();

        assert_eq!(0, dict.len());

        iter.for_each(|_| panic!("Iterated over non-existing item"));
    }

    #[test]
    fn test_iter_cstr() {
        let (_strings, _items, raw) = make_raw_dict(2);
        let dict = unsafe { Dict::from_ptr(&raw) };

        let mut iter = dict.iter_cstr();
        assert_eq!(
            (
                CString::new("K0").unwrap().as_c_str(),
                CString::new("V0").unwrap().as_c_str()
            ),
            iter.next().unwrap()
        );
        assert_eq!(
            (
                CString::new("K1").unwrap().as_c_str(),
                CString::new("V1").unwrap().as_c_str()
            ),
            iter.next().unwrap()
        );
        assert_eq!(None, iter.next());
    }

    #[test]
    fn test_iterators() {
        let (_strings, _items, raw) = make_raw_dict(2);
        let dict = unsafe { Dict::from_ptr(&raw) };

        let mut iter = dict.iter();
        assert_eq!(("K0", "V0"), iter.next().unwrap());
        assert_eq!(("K1", "V1"), iter.next().unwrap());
        assert_eq!(None, iter.next());

        let mut key_iter = dict.keys();
        assert_eq!("K0", key_iter.next().unwrap());
        assert_eq!("K1", key_iter.next().unwrap());
        assert_eq!(None, key_iter.next());

        let mut val_iter = dict.values();
        assert_eq!("V0", val_iter.next().unwrap());
        assert_eq!("V1", val_iter.next().unwrap());
        assert_eq!(None, val_iter.next());
    }

    #[test]
    fn test_get() {
        let (_strings, _items, raw) = make_raw_dict(1);
        let dict = unsafe { Dict::from_ptr(&raw) };

        assert_eq!(Some("V0"), dict.get("K0"));
    }

    #[test]
    fn test_debug() {
        let (_strings, _items, raw) = make_raw_dict(1);
        let dict = unsafe { Dict::from_ptr(&raw) };

        assert_eq!(
            r#"Dict { flags: (empty), items: {"K0": "V0"} }"#,
            &format!("{:?}", dict)
        )
    }

    #[test]
    fn dict_item() {
        let item = DictItem::new("key", "value");
        assert_eq!(item.key(), CStr::from_bytes_with_nul(b"key\0").unwrap());
        assert_eq!(item.value(), CStr::from_bytes_with_nul(b"value\0").unwrap());
    }

    #[test]
    fn owned_dict() {
        let mut dict = OwnedDict::new();
        assert_eq!(dict.len(), 0);
        dict.insert("key", "value");
        assert_eq!(dict.len(), 1);
        dict.insert("key2", "value");
        dict.insert("key3", "value");
        assert_eq!(dict.len(), 3);
        dict.insert("key", "new_value");
        assert_eq!(dict.len(), 3, "{:?}", dict);
        assert!(dict.remove("key").is_some());
        assert_eq!(dict.len(), 2);
        assert!(dict.remove("made up").is_none());
        assert_eq!(dict.len(), 2);
    }
}
